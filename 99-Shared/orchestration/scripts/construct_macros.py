# Constructor for building the Operator level RF Overview OPI from SystemExpert
# dictionary files.

# Requirement : SystemExpert RF repo is cloned to:
#           /opt/opis/20-SystemExpert/10-ACC/999-RF/

# import all macro-building files
import _NCL1, _MEBTbunchers, _NCL2, _NCL3

groups =    {
            # "_NCL1" : _NCL1.get_macros(),
            # "_MEBTbunchers" : _MEBTbunchers.get_macros(),
            "_NCL2" : _NCL2.get_macros(),
            "_NCL3" : _NCL3.get_macros(),
            }

for group,macros in groups.items():
    print("\nGroup : {}\n".format(group))
    print("<macros>")
    for k,v in macros.items():
        print("<{}>{}</{}>".format(k,v,k))
    print("</macros>")
    print("\n")
