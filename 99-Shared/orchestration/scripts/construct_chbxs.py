systems =   [
            ["RFQ","DTL1"],
            ["MEBT"],
            ["DTL2","DTL3"],
            ["DTL4","DTL5"],
            ]

def return_chbox_wdg(i,system):
    print('<widget type="checkbox" version="2.0.0">')
    print('  <name>ch_{}</name>'.format("+".join(system)))
    print('  <pv_name>loc://RF_Overview_{}(0)</pv_name>'.format("_".join(system)))
    print('  <label>{}</label>'.format("+".join(system)))
    print('  <x>30</x>')
    print('  <y>{}</y>'.format(130+i*40))
    print('  <width>110</width>')
    print('  <height>30</height>')
    print('  <enabled>false</enabled>')
    print('</widget>')


for i,system in enumerate(systems):
    return_chbox_wdg(i,system)
